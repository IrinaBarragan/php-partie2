<?php
// On démarre la session AVANT d'écrire du code HTML
session_start();

// On s'amuse à créer quelques variables de session dans $_SESSION
$_SESSION['prenom'] = 'Jean';
$_SESSION['nom'] = 'Dupont';
$_SESSION['age'] = 24;
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>SESSION</title>
    </head>
    <body>
    <p>
        Salut <?php echo $_SESSION['prenom']. ' '. $_SESSION['nom']. ' '. $_SESSION['age']; ?> !<br />
        
        Tu veux aller sur une autre page ?
    </p>
    <p>
        <a href="user.php">Lien vers mapage.php</a><br />

    </p>
    </body>
</html>